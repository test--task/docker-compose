package flask 

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class flasktest extends Simulation {

  val httpProtocol = http
    .baseUrl("http://flask:5000") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xml") // Here are the common headers
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

    val scn = scenario("Flask Test") // A scenario is a chain of requests and pauses
      .repeat (10) { 
        exec(http("check root")
        .get("/")) 
        .pause(1) // Note that Gatling has recorded real time pauses
      }

    setUp(scn.inject(atOnceUsers(1)).protocols(httpProtocol))
}

