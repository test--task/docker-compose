FROM python:alpine

LABEL name="flask demo" \
      version=1

WORKDIR /
ENV testvariable 1
ENV production true

RUN pip install flask_demo 

EXPOSE 8080

CMD "run_flask_demo"

